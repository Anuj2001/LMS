from rest_framework import serializers
from .models import *
from django.core.exceptions import ValidationError



class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegUser
        fields = '__all__'



class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class DeleteUserSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    def validate_user_id(self, value):        
        try:
            user = RegUser.objects.get(id=value)
        except RegUser.DoesNotExist:
            raise ValidationError("User with ID {user} does not exist.".format(value))
        return value
    


class CourseAddSerializer(serializers.ModelSerializer):   
    class Meta:
        model = AddCourse
        fields = '__all__'


class TrainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainer
        fields = '__all__'

class OptedCoursesSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptedCourse
        fields = '__all__'


