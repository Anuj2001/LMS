from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone


class CustomUserManager(BaseUserManager):
    def create_user(self, uname, password, **extra_fields):
        if not uname:
            raise ValueError('The Username field must be set')
        user = self.model(uname=uname, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, uname, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(uname, password=None,**extra_fields)
    


class RegUser(AbstractBaseUser):
    CHOICES = (
        ('Yes', 'Yes'),
        ('No', 'No')
    )
    VERTICAL_CHOICES = (
        ('SUB', 'SUB'),
        ('SUI', 'SUI'),
        ('SIRT', 'SIRT'),
        ('SIRTE', 'SIRTE'),
    )
    GENDER_CHOICES = (
        ('M', 'M'),
        ('F', 'F'),
    )

    uname = models.CharField(max_length=100, unique=True)
    fname = models.CharField(max_length=100, default='')
    password = models.CharField(max_length=128, default='')
    phonenum = models.CharField(max_length=100, default='')
    ref_name = models.CharField(max_length=100, default='')
    email = models.EmailField(default=None, null=True)
    isSageian = models.CharField(max_length=50, choices=CHOICES, default='Yes')
    role = models.IntegerField(default=1, db_comment='1=student, 2=admin, 0=coordinator')
    # last_login = models.DateTimeField(auto_now=True)
    Country = models.CharField(max_length=255, default='')
    state = models.CharField(max_length=255, default='')
    City = models.CharField(max_length=255, default='')
    created_at = models.DateField(auto_now=True)
    vertical = models.CharField(max_length=100, default='', choices=VERTICAL_CHOICES)
    Gender = models.CharField(max_length=1, default='M', choices=GENDER_CHOICES)
    otp = models.IntegerField(default=0)
    USERNAME_FIELD = 'uname'
    objects = CustomUserManager()

    def __str__(self):
        return self.uname

#class RegUser(models.Model):
#     uname = models.CharField(max_length=100, default="")

class AddCourse(models.Model):
    CATEGORY_CHOICES = [
        ('1', 'Science and Technology'),
        ('2', 'Life Enrichment'),
        ('3', 'Skill Development'),
        ('4', 'Management and Finance'),
    ]
    
    VERTICAL_CHOICES = [
        ('SUB', 'SUB'),
        ('SUI', 'SUI'),
        ('SIRT', 'SIRT'),
        ('SIRTE', 'SIRTE'),
    ]
    
    COURSE_MODE_CHOICES = [
        (0, 'Online'),
        (1, 'Offline'),
    ]
    coord_id = models.IntegerField(default='0')
    course_title = models.CharField(max_length=255)
    course_category = models.CharField(max_length=100, choices=CATEGORY_CHOICES)
    vertical = models.CharField(max_length=100, choices=VERTICAL_CHOICES, default="" )
    course_mode = models.IntegerField(choices=COURSE_MODE_CHOICES, default="", db_comment='1=offline, 0=online')
    course_img = models.FileField(upload_to='images/', default='default.jpg', blank=True, null=True)
    url = models.CharField(max_length=255, default="")
    description = models.TextField(default="")
    syllabus = models.TextField(default="")
    outcome = models.TextField(default="")
    coord_mob = models.CharField(max_length=20, default="")
    coord_name = models.CharField(max_length=100, default="")
    coord_mail = models.EmailField(default="")
    startdate = models.DateField(default=None, null=True)
    enddate = models.DateField(default=None, null=True)
    course_status = models.IntegerField(default=10, db_comment='0=New Added, 1=Approved, 2=Changes Required, 3=Disapproved, 4=Complete')
    def __str__(self):
        return self.course_title
    


class OptedCourse(models.Model):
    student = models.ForeignKey(RegUser, on_delete=models.CASCADE)
    course = models.ForeignKey(AddCourse, on_delete=models.CASCADE)
    order_id = models.TextField(default="")
    # pay_ref_no = models.IntegerField(default="")
    # pay_date = models.DateField(default=timezone.now) 
    attendance = models.TextField(default="")
    # quizz_marks = models.IntegerField(default="")
    assignment_marks = models.IntegerField(default=0)
    batch_name = models.TextField(default="")
    # status = models.IntegerField(default="")
    coordinator_assessment = models.TextField(default="")


class Trainer(models.Model):
    Trainer_name = models.CharField(max_length=100, default="")
    Trainer_email = models.EmailField(max_length=100, default="")
    Trainer_number = models.TextField(max_length=100, default="")
    course_id = models.TextField(max_length=100, default="")
    Trainer_img = models.FileField(upload_to='trainer/', default='default.jpg', blank=True, null=True)


class PymntCompleted(models.Model):
    StudentID = models.IntegerField(default="")
    order_ID = models.TextField(max_length=100, default="")
    fee = models.IntegerField(default="")
    rcv_amount = models.IntegerField(default="")
    vertical = models.IntegerField(default="")
    course_title = models.TextField(max_length=100, default="")
    paymnt_mode = models.CharField(max_length=100, default="")
    paymnt_status = models.TextField(max_length=100, default="")
    message = models.TextField(max_length=100, default="")
    gateway_name = models.TextField(max_length=100, default="")
    bank_txn_id = models.IntegerField(default="")
    bank_name = models.TextField(max_length=100, default="")
    txn_date_and_time = models.DateTimeField(default="")


class Attendance(models.Model):
    student = models.ForeignKey(RegUser, on_delete=models.CASCADE)
    course = models.ForeignKey(AddCourse, on_delete=models.CASCADE)
    date = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    attendance = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    def __str__(self):
        return f'{self.student} - {self.course} - {self.date}'
    
   


class Country(models.Model):
    sortname = models.CharField(max_length=30)
    name = models.CharField(max_length=255)
    phonecode = models.IntegerField()

    def __str__(self):
        return self.name

class State(models.Model):
    name = models.CharField(max_length=255)
    country_id = models.IntegerField(default='0')

    def __str__(self):
        return self.name

class City(models.Model):
    name = models.CharField(max_length=255)
    state_id = models.IntegerField(default='0')

    def __str__(self):
        return self.name