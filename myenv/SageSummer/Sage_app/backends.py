from django.contrib.auth.backends import BaseBackend
from .models import *
from django.core.exceptions import MultipleObjectsReturned  
import hashlib, bcrypt

class RegisterUserBackend(BaseBackend):
    def authenticate(self, request, uname=None, password=None, hashed_password=None):
        try:
            user = RegUser.objects.get(uname=uname)
            # role = user.role
            # uname = user.uname
            # gender = user.Gender
            # return user, role, uname, gender
            hashed_password = user.password.encode() 
            if bcrypt.checkpw(password.encode(), hashed_password):
                role = user.role
                uname = user.uname
                gender = user.Gender
                vertical = user.vertical
                return user, role, uname, gender, vertical
            else:   
                return None, None, None, None, None
        except RegUser.DoesNotExist:
            return None, None, None, None, None
        except MultipleObjectsReturned:            
            return None, None, None, None, None

    def get_user(self, id):
        try:
            return RegUser.objects.get(pk=id)
        except RegUser.DoesNotExist:
            return None
        
