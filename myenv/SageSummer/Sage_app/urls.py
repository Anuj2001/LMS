from django.urls import path
from . import views
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('coordinator/add-coordinator', views.add_coordinator),
    path('coordinator/coordinator-data', views.coordinator_data),
    path('register/', views.Register_user),
    path('login/',views.Login),
    path('csrf_cookie/', views.csrf_cookie , name='csrf_cookie'),
    path('delete-user/<int:user_id>/', Delete_user),
    path('coordinator/course-add/', views.Course_add),
    path('coordinator/course/<int:userID>/', views.CoordCourse),
    path('coordinator/add-trainer/<int:course_id>/', views.AddTrainer),
    path('coordinator/std-attendance/<int:course_ID>/', views.StudentAttendance),
    path('all-approval-courses/', views.AllApprovalCourses),
    path('all-courses/<int:userId>/', views.All_Courses),
    path('total-courses-std/', views.Total_courses),
    path('update-cour-status/<int:selectedCourseId>/', views.UpdateCourStatus),
    path('join-course/<int:stdid>/<int:courseid>/', views.JoinCourse),
    path('opted-courses/<int:userID>/', views.StdOptedCourse),
    path('resetpassword/', views.ResetPassword),
    path('OTP/',views.Otp),
    path('course/edit/<int:id>/',views.Edit_course),
    path('selected-courses/<int:id>/', views.DisplayEditCourse),
    path('update/std-attendance/<int:course_ID>/', views.UpdateStdAttendance),
    path('student/attendance/<int:course_ID>/', views.TotalAttendance),
    path('student/remove-course/<int:optID>/', views.StdRemoveCourse),
    path('student-list/<int:courseid>/', views.StudentList),
    path('country/code/', views.CountryCode),
    path('state/<int:selectedCountryId>', views.StateCode),
    path('city/<int:selectedStateId>', views.CityCode),
    
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


