import random, string, hashlib, bcrypt, base64, io
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from .models import *
from .serializers import *
from .backends import *
from rest_framework.decorators import api_view, parser_classes, permission_classes
from rest_framework import status
from django.contrib.auth import authenticate, login
from rest_framework.response import Response
from django.middleware.csrf import get_token
from rest_framework.permissions import AllowAny
from rest_framework.parsers import MultiPartParser, FormParser
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from django.core import serializers
from PIL import Image
from datetime import datetime
from django.db.models import Value
from django.db.models.functions import Concat
from django.shortcuts import get_object_or_404



@csrf_exempt
@api_view(['POST'])
def add_coordinator(request):
    if request.method == 'POST':
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():
            uname = serializer.validated_data.get('uname')
            email = serializer.validated_data.get('email')
            
            if RegUser.objects.filter(uname=uname, email=email).exists():
                return Response({'error': 'Username already exists'}, status=status.HTTP_400_BAD_REQUEST)
            password = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
            hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

            send_mail(
                'Your Password for Coordinator Account',
                f'Your password is: {password}',
                'smilekeep700@gmail.com',  
                [email],
                fail_silently=False,
            )
            serializer.save(password=hashed_password)
            new_user = RegUser.objects.get(uname=uname, email=email)           
            user_serializer = RegisterUserSerializer(new_user)
            return Response({'message': 'User registered successfully', 'data': user_serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({'error': 'Method Not Allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)


#this function is for get data from coordinator
@api_view(['GET', 'POST'])
def coordinator_data(request):
    if request.method == 'GET':
        data = RegUser.objects.filter(role=0)
        data_list = list(data.values())
        return JsonResponse({'data': data_list}, safe=False)
    else:
        return JsonResponse({'message': 'Method Not Allowed'}, status=405)
    



@api_view(['GET'])
def CoordCourse(request, userID):
    try:       
        courses = AddCourse.objects.filter(coord_id=userID)                
        courses_data = []
        if courses.exists():
            courses_data = serializers.serialize('json', courses)
            #print('==2==',courses_data)
            return JsonResponse({'data': courses_data}, safe=False)
        else:
            return JsonResponse({'message': 'No courses found for the coordinator'}, status=404)
    except AddCourse.DoesNotExist:
        return JsonResponse({'message': 'No courses found for the coordinator'}, status=404)
    

@api_view(['POST'])
@parser_classes([MultiPartParser, FormParser])
def AddTrainer(request, course_id):
    if request.method == 'POST':
        mutable_data = request.data.copy()
        # print("============================1")
        # Check if 'Trainer_img' exists in the data
        if 'Trainer_img' in mutable_data:
            img_file = mutable_data['Trainer_img']
            # print("============================2")
        # Serialize the modified data
        serializer = TrainerSerializer(data=mutable_data)
        # print('====',serializer)
        # Check if the data is valid
        if serializer.is_valid():
            # Save the data
            # print("============================3")
            serializer.save(course_id=course_id)
            return Response(serializer.data, status=201)
        else:
            # Return errors if the data is not valid
            errors = serializer.errors
            return Response(errors, status=400)
    else:
        # Return method not allowed response for other methods
        return Response({"message": "Method not allowed"}, status=405)

@api_view(['POST'])
def Login(request):
    if request.method == 'POST':
        uname = request.data.get('username')
        password = request.data.get('password')
        # hashed_password = hashlib.md5(password.encode()).hexdigest()
        # print('============password==============', hashed_password)
        
        user, role, uname, Gender, vertical = RegisterUserBackend().authenticate(request, uname=uname, password=password)
        if user is not None:
            login(request, user, backend='Sage_app.backend.RegisterUserBackend')
            
            request.session['userid'] = user.id
            request.session['role'] = role
            request.session['gender'] = Gender
            request.session['uname'] = uname
            request.session['vertical'] = vertical
            request.session.save()

            serializer = RegisterUserSerializer(user)
            serialized_data = serializer.data
            serialized_data['userid'] = user.id
            serialized_data['role'] = role
            serialized_data['gender'] = Gender
            serialized_data['uname'] = uname
            serialized_data['vertical'] = vertical
            # print("================================", serialized_data)
            return JsonResponse({'message': 'Login successful', 'user': serialized_data}, status=200)
        else:
            return JsonResponse({'message': 'Invalid credentials'}, status=401)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

@ensure_csrf_cookie
def csrf_cookie(request):
    csrf_token = get_token(request)
    return JsonResponse({'csrfToken': csrf_token})


@api_view(['POST'])
def Register_user(request):
    if request.method == 'POST':
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():

            uname = serializer.validated_data.get('uname')
            email =serializer.validated_data.get('email')
            password = serializer.validated_data.get('password')
            hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()
            if RegUser.objects.filter(uname=uname,  email=email).exists():
                return Response({'error': 'Username already exists'}, status=status.HTTP_400_BAD_REQUEST)
            user = serializer.save(password=hashed_password)
            request.session['id'] = user.id
            request.session['role'] = serializer.validated_data.get('role')
            request.session['uname'] = uname
            request.session.save()  
            serialized_data = serializer.data            
            response_data = {
                'message': 'User registered successfully',
                'user': {
                    'id': user.id,
                    'role': user.role,
                    'uname': user.uname,
                }
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    else:

        return Response({'error': 'Method Not Allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['DELETE'])
def Delete_user(request, user_id):
    if request.method == 'DELETE':
        serializer = DeleteUserSerializer(data={'user_id': user_id})
        if serializer.is_valid():
            user_id = serializer.validated_data['user_id']
            try:
                user = RegUser.objects.get(id=user_id)
                user.delete()
                return Response({'message': 'User deleted successfully'}, status=200)
            except RegUser.DoesNotExist:
                return Response({'error': 'User not found'}, status=404)
        else:
            return Response(serializer.errors, status=400)
    else:
        return Response({'error': 'Method not allowed'}, status=405)

@api_view(['POST'])
@parser_classes([MultiPartParser, FormParser])
def Course_add(request):
    if request.method == 'POST':
        mutable_data = request.data.copy()
        # print("============", request.data)
        # Check if 'course_img' exists in the data
        if 'course_img' in mutable_data:
           
            img_file = mutable_data['course_img']
            
        # Serialize the modified data
        serializer = CourseAddSerializer(data=mutable_data)
        
        # Check if the data is valid
        if serializer.is_valid():
            # Save the data
            
            serializer.save()
            return Response(serializer.data, status=201)
        else:
            # Return errors if the data is not valid
            errors = serializer.errors
            return Response(errors, status=400)


@api_view(['GET'])
def AllApprovalCourses(request):
    if request.method =='GET':
        courses = list(AddCourse.objects.values())
        # serialized_courses = serializers('json', courses)
        return JsonResponse({'courses':courses})
    else:
        return JsonResponse({'message':'method not allowed'}, status=405)



@api_view(['GET'])
def All_Courses(request, userId):
    if request.method == 'GET':
        data = AddCourse.objects.all()
        data_list = list(data.values())
        if userId:
            opted_courses = OptedCourse.objects.filter(student=userId)
            opted_course_ids = list(opted_courses.values_list('course_id', flat=True))
            unopted_courses = [course for course in data_list if course['id'] not in opted_course_ids]
            
            return JsonResponse({'data': unopted_courses}, safe=False)
        else:
            return JsonResponse({'message': 'User ID not provided'}, status=400)
    else:
        return JsonResponse({'message': 'Method Not Allowed'}, status=405)



def Total_courses(request):
    total_num_courses = AddCourse.objects.count()
    total_std = RegUser.objects.filter(role=1).count()
    total_approved_course = AddCourse.objects.filter(course_status=1).count()
    total_courses = list(AddCourse.objects.values())
    total_coord = RegUser.objects.filter(role=0).count()
    return JsonResponse({'total_coord':total_coord, 'total_num_courses': total_num_courses, 'total_students': total_std, 'total_courses': total_courses, 'total_approved_course':total_approved_course })


@api_view(['PUT'])
def UpdateCourStatus(request, selectedCourseId):
    if request.method == "PUT":
        # Retrieve the course object using the provided selectedCourseId
        try:
            course = AddCourse.objects.get(pk=selectedCourseId)
        except AddCourse.DoesNotExist:
            return JsonResponse({'error': 'Course not found'}, status=404)
        # Update the status of the course
        new_status = request.data.get('status')
        if new_status is not None:
            course.course_status = new_status
            course.save()
            return JsonResponse({'message': 'Course status updated successfully'})

    return JsonResponse({'error': 'Invalid request method'}, status=400)


@api_view(['POST'])
def JoinCourse(request, stdid, courseid):
    try:   
        student = RegUser.objects.get(id=stdid)
        course = AddCourse.objects.get(id=courseid)         
        if OptedCourse.objects.filter(student=stdid, course=courseid).exists():
            return JsonResponse({'message': 'User has already joined this course'}, status=400)                                    
        OptedCourse.objects.create(student=student, course=course)

        # Fetch joined courses after joining a new course
        opted_courses = OptedCourse.objects.filter(student=student)
        opted_courses_data = []
        for course in opted_courses:
            course_data = {
                'course_id': course.course.id,
                'course_name': course.course.course_title,  # Add more fields as needed
                # Include other course details you want to return
            }
            opted_courses_data.append(course_data)

        return JsonResponse({
            'message': 'Student enrolled in course successfully.',
            'joined_courses': opted_courses_data
        })
    except (RegUser.DoesNotExist, AddCourse.DoesNotExist) as e:
        return JsonResponse({'error': f'Error joining course: {e}'}, status=400)
    except Exception as e:
        return JsonResponse({'error': f'Error: {e}'}, status=400)


@api_view(['GET'])
def StdOptedCourse(request, userID):
    try:
        opted_courses = OptedCourse.objects.filter(student=userID)
        opted_courses_data = []
        for course in opted_courses:
            course_data = {
                'id':course.id,
                'course_id': course.course.id,                
                'course_data': list(AddCourse.objects.filter(id=course.course.id).values())
            }
            opted_courses_data.append(course_data)
        return JsonResponse(opted_courses_data, safe=False)
    except OptedCourse.DoesNotExist:
        return JsonResponse({'message': f'No opted courses found for user ID {userID}'}, status=404)


@api_view(['POST'])
@csrf_exempt
@permission_classes([AllowAny])
def ResetPassword(request):
    if request.method == 'POST':
        # Get OTP and new password from request data
        otp = request.data.get('otp')
        new_password = request.data.get('newPassword')  # Correct parameter name
        if new_password is not None:  # Check if new_password is not None
            user = RegUser.objects.filter(otp=otp).first()
            if user:
                # Encode the new password before hashing
                encoded_password = new_password.encode('utf-8')
                # Update user's password with the new password
                user.password = bcrypt.hashpw(encoded_password, bcrypt.gensalt()).decode()
                user.save()
                # Return success response
                return JsonResponse({'success': True})
            else:
                # Return error response if OTP doesn't match
                return JsonResponse({'success': False, 'error': 'Invalid OTP'}, status=400)
        else:
            # Return error response if new_password is not provided
            return JsonResponse({'success': False, 'error': 'New password is required'}, status=400)
    else:
        # Return error response for invalid request method
        return JsonResponse({'error': 'Invalid request method'}, status=405)

@api_view(['POST'])
def Otp(request):
    if request.method == 'POST':
        email = request.data.get('email')
        # print("==================================================",email)
        # Generate OTP
        otp = get_random_string(length=6, allowed_chars='1234567890')
        # Send OTP via Email        
        send_mail(
            'Your Password for Coordinator Account',
            f'Your OTP is: {otp}',
            'smilekeep700@gmail.com',  
            [email],
            fail_silently=False,
        )  
        try:
            reg_users = RegUser.objects.filter(email=email)
        except RegUser.DoesNotExist:
            # Handle case where no user exists with the given email
            return JsonResponse({'error': 'User does not exist'}, status=404)


        # Update the otp for the user
        for reg_user in reg_users:
            reg_user.otp = otp
            reg_user.save()
        return JsonResponse({'message': '1'})
    else:
        return JsonResponse({'error': 'Invalid request method'}, status=405)
    

@api_view(['PUT'])
def Edit_course(request, id):
    if request.method == 'PUT':
        course_id = id  
        # print("===========")
        # Assuming you receive the updated course data in the request body
        updated_data = request.data  # Assuming the request body contains the updated course data
        try:
            course = AddCourse.objects.get(id=course_id)
            # Update the course object with the new data
            for key, value in updated_data.items():
                setattr(course, key, value)
            course.save()  # Save the updated course object
            return JsonResponse({'message': 'Course updated successfully'}, status=200)
        except AddCourse.DoesNotExist:
            return JsonResponse({'error': 'Course not found'}, status=404)
    else:
        return JsonResponse({'error': 'Method Not Allowed'}, status=405)


@api_view(['GET'])
def DisplayEditCourse(request, id):
    if request.method == 'GET':
        try:
            # print("=======id of course",id)
            # Retrieve the course based on the provided ID
            course = AddCourse.objects.filter(id=id)
            # Check if the course exists
            if course.exists():
                # Serialize the queryset to JSON
                # print("==========data====",course)
                course_data = serializers.serialize('json', course)
                # print("==========data====",course_data)
                return JsonResponse({'data': course_data}, safe=False)
            else:
                # If the course with the provided ID does not exist, return an error response
                return JsonResponse({'error': f'Course with ID {id} does not exist'}, status=404)
        except Exception as e:
            # Return a server error response if an exception occurs
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'error': 'Method Not Allowed'}, status=405)


@api_view(['GET'])
def StudentAttendance(request, course_ID):
    if request.method == 'GET':
        courseID = course_ID
        opted_students = OptedCourse.objects.filter(course_id=courseID).values_list('student', flat=True)
        course_d = OptedCourse.objects.filter(course_id=courseID).values_list('course', flat=True)
        course_data = AddCourse.objects.filter(id__in=course_d).values('startdate', 'enddate')

        # Ensure end date is not before start date
        for course in course_data:
            if course['enddate'] < course['startdate']:
                course['enddate'] = course['startdate']

        coursedata = list(course_data)

        student_data = RegUser.objects.filter(id__in=opted_students).values('id', 'uname')
        stddata = list(student_data)

        return JsonResponse({'student_data': stddata, 'course_data': coursedata}, safe=False)
    else:
        return JsonResponse({'message': 'Method not allowed'}, status=405)
    

@api_view(['POST'])
def UpdateStdAttendance(request, course_ID):
    if request.method == 'POST':
        data = request.data.get('attendanceData')
        user = request.data.get('user')
        
        # List to store dates with True attendance
        true_attendance_dates = []

        for entry in data:
            student_id = entry['student_id']
            attendance_list = entry['attendance']
            print("==============", student_id, attendance_list)
            # Iterate through attendance list
            for attendance_entry in attendance_list:
                if attendance_entry['attended']:
                    true_attendance_dates.append(attendance_entry['date'])

            if student_id and course_ID:
                existing_attendance = Attendance.objects.filter(student=student_id, course=course_ID).first()

                if existing_attendance:
                    existing_data = existing_attendance.date
                    
                    new_date_data = existing_data + ', ' + ', '.join(true_attendance_dates)
                   
                    existing_attendance.date = new_date_data
                    # dataattendance = sorted(set(existing_attendance))
                    existing_attendance.save()
                else:
                    # Create a new attendance record
                    try:
                        # Get the student instance
                        student = RegUser.objects.get(id=student_id)                        
                        # Create a new attendance record
                        Attendance.objects.create(student=student, course_id=course_ID, date=', '.join(true_attendance_dates), created_by=user)
                    except RegUser.DoesNotExist:
                       
                        pass

        return JsonResponse({'message': 'Attendance updated successfully'}, status=200)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)
    

@api_view(['GET'])
def TotalAttendance(request, course_ID):
    if  request.method == 'GET':
        data = Attendance.objects.filter( course=course_ID).values()
        data = list(data)
        # print("========data=======", data)
        return JsonResponse({'data': data}, safe=False)
    else:
        return JsonResponse({'message': 'Invalid request method'}, status=400)
    
@api_view(['DELETE'])
def StdRemoveCourse(request, optID):
    if request.method == 'DELETE':
        # Get the OptedCourse object based on the optID
        opted_course = get_object_or_404(OptedCourse, id=optID)

        # Delete the OptedCourse
        opted_course.delete()

        # Return a success response
        return JsonResponse({'message': 'Course removed successfully.'}, status=204)
    else:
        # Return a method not allowed response
        return JsonResponse({'error': 'Method not allowed.'}, status=405)
    


@api_view(['GET'])
def StudentList(request, courseid):
    if request.method == 'GET':
        course = OptedCourse.objects.filter(course=courseid).values()
        student_ids = course.values_list('student', flat=True)
        students = RegUser.objects.filter(id__in=student_ids).values('id', 'uname', 'fname', 'phonenum', 'email', 'vertical', 'Gender', 'created_at')
        # print(student_ids)
        data = list(students)
        # print(data)
        return JsonResponse({'data':data}, safe=False)
    return JsonResponse("hello")



@api_view(['GET'])
def CountryCode(request):
    if request.method == 'GET':
        countries = Country.objects.all().values()
        country_data = list(countries)
        return JsonResponse({'Country_data': country_data})
    else:
        return JsonResponse()

@api_view(['GET'])
def StateCode(request, selectedCountryId):
    if request.method == 'GET':
        states = State.objects.filter(country_id=selectedCountryId).values()
        states_data = list(states)
        return JsonResponse({'states_data': states_data})
    else:
        return JsonResponse()

@api_view(['GET'])
def CityCode(request, selectedStateId):
    if request.method == 'GET':
        cities = City.objects.filter(state_id=selectedStateId).values()
        cities_data = list(cities)
        return JsonResponse({'cities_data': cities_data})
    else:
        return JsonResponse()