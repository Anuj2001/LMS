from django.apps import AppConfig


class SageAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Sage_app'
