import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import useAuth from '../useAuth';
const Base = () => {
    const { logout, role: authRole } = useAuth();
    const [sessionRole, setSessionRole] = useState('');
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [screenSize, setScreenSize] = useState('lg');
    const [dataSide, setDataSideBarSize] = useState('lg');

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
        setScreenSize(screenSize === 'lg' ? 'sm' : 'lg');
        setDataSideBarSize(screenSize === 'lg' ? 'sm' : 'lg');
        const hamburgerIcon = document.getElementById('topnav-hamburger-icon');
        hamburgerIcon.querySelector('.hamburger-icon').classList.toggle('open');
    };
   
    useEffect(() => {       
        const htmlElement = document.querySelector('html');
        htmlElement.setAttribute('data-sidebar-size', dataSide);
    }, [dataSide]);

    useEffect(() => {
        const checkScreenSize = () => {
            const screenWidth = window.innerWidth;
            setScreenSize(screenWidth <= 768 ? 'sm' : 'lg');
            setDataSideBarSize(screenWidth <= 768 ? 'sm' : 'lg');
        };

        window.addEventListener('resize', checkScreenSize);
        checkScreenSize(); 

        return () => {
            window.removeEventListener('resize', checkScreenSize);
        };
    }, []);

    
    const handleLogout = () => {
        logout();
        window.location.href = '/Login';
    };

    useEffect(() => {
        const userRole = sessionStorage.getItem('userRole');
        const uname = sessionStorage.getItem('uname');
        const gender = sessionStorage.getItem('gender');
        const userid = sessionStorage.getItem('userid')
        setSessionRole(userRole);
        // console.log("======================", uname, userRole, gender);
    }, []);
    
    
    return (
        <>
            <header id="page-topbar">
                <div class="layout-width">
                    <div class="navbar-header">
                        <div class="d-flex">

                            <div class="navbar-brand-box horizontal-logo">
                                <a href="index-2.html" class="logo logo-dark">
                                    <span class="logo-sm">
                                        <img src="assets/images/logo-sm.png" alt="" height="22" />
                                    </span>
                                    <span class="logo-lg">
                                        <img src="assets/images/logo-dark.png" alt="" height="17" />
                                    </span>
                                </a>
                                <a href="index-2.html" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img src="assets/images/logo-sm.png" alt="" height="22" />
                                    </span>
                                    <span class="logo-lg">
                                        <img src="assets/images/logo-light.png" alt="" height="17" />
                                    </span>
                                </a>
                            </div>
                            <button type="button" class="btn btn-sm px-3 fs-16 header-item vertical-menu-btn topnav-hamburger shadow-none" id="topnav-hamburger-icon" onClick={toggleMenu}>
                                <span class={`hamburger-icon ${isMenuOpen ? 'open' : ''}`}>                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
                            </button>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="ms-1 header-item d-none d-sm-flex">
                                <button type="button"
                                    class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle light-dark-mode shadow-none">
                                    <i class="bx bx-moon fs-22"></i>
                                </button>
                            </div>

                            <div class="dropdown ms-sm-3 header-item topbar-user">
                                <button type="button" class="btn shadow-none" id="page-header-user-dropdown"
                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="d-flex align-items-center">
                                        {sessionStorage.getItem('gender') === 'F' && (
                                            <img class="rounded-circle header-profile-user"
                                                src="assets/images/users/avatar-1.jpg" alt="Male Avatar" />
                                        )}
                                        {sessionStorage.getItem('gender') === 'M' && (
                                            <img class="rounded-circle header-profile-user"
                                                src="assets/images/users/avatar-3.jpg" alt="Female Avatar" />
                                        )}
                                        <span class="text-start ms-xl-2">
                                            <span class="d-none d-xl-inline-block ms-1 fw-medium user-name-text">
                                                {sessionStorage.getItem('uname')}
                                            </span>
                                            {sessionStorage.getItem('userRole') === '1' && (
                                                <span class="d-none d-xl-block ms-1 fs-12 text-muted user-name-sub-text">Student</span>
                                            )}
                                            {sessionStorage.getItem('userRole') === '2' && (
                                                <span class="d-none d-xl-block ms-1 fs-12 text-muted user-name-sub-text">Admin</span>
                                            )}
                                            {sessionStorage.getItem('userRole') === '0' && (
                                                <span class="d-none d-xl-block ms-1 fs-12 text-muted user-name-sub-text">Coordinator</span>
                                            )}
                                        </span>
                                    </span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <h6 class="dropdown-header">Welcome {sessionStorage.getItem('uname')} !</h6>
                                    {/* <a class="dropdown-item" href="pages-profile.html">
                                        <i class="mdi mdi-account-circle text-muted fs-16 align-middle me-1"></i>
                                        <span class="align-middle">Profile</span>
                                    </a> */}
                                    <a class="dropdown-item" onClick={handleLogout}>
                                        <i class="mdi mdi-logout text-muted fs-16 align-middle me-1"></i>
                                        <span class="align-middle" data-key="t-logout">Logout</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class='app-menu navbar-menu'>
                <div class="navbar-brand-box">
                    <a class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="assets/images/logo-sm.png" alt="" height="22" />
                        </span>
                        <span class="logo-lg">
                            <img src="assets/images/logo-dark.png" alt="" height="17" />
                        </span>
                    </a>

                    <a href="index-2.html" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="assets/images/logo-sm.png" alt="" height="22" />
                        </span>
                        <span class="logo-lg">
                            <img src="assets/images/logo-light.png" alt="" height="17" />
                        </span>
                    </a>
                    <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
                        id="vertical-hover">
                        <i class="ri-record-circle-line"></i>
                    </button>
                </div>
                <div id="scrollbar">
                    <div class="container-fluid">
                        <div id="two-column-menu"></div>
                        <ul class="navbar-nav" id="navbar-nav">
                            {/* Role 0 means coordinator */}
                            {(sessionRole === '0' || sessionRole === 0) && (
                                <>
                                    <li class="nav-item">
                                        <Link to="/Dashboard" class="nav-link menu-link">
                                            <i class="ri-home-fill"></i>
                                            <span>
                                                Dashboard
                                            </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/All-Courses">
                                            <i class="ri-align-justify"></i>
                                            <span> All Courses </span>
                                        </Link>
                                    </li>

                                    {/* <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/Student-Attendance"  >
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span> Courses Attendance</span>
                                        </Link>
                                    </li> */}

                                    <li class="nav-item">
                                        <Link to="/Holidays-list" class="nav-link menu-link">
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span>
                                                Holiday List
                                            </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/Add-course" class="nav-link menu-link">
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span>
                                                Add Course
                                            </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/Your-courses" class="nav-link menu-link">
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span>
                                                Your Courses
                                            </span>
                                        </Link>
                                    </li>

                                </>
                            )}
                            {/* Role 2 means Admin */}
                            {(sessionRole === '2' || sessionRole === 2) && (
                                <>
                                    <li class="menu-title">
                                        <span data-key="t-menu">User Dashboard</span>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/Dashboard" class="nav-link menu-link">
                                            <i class="ri-home-fill"></i>
                                            <span>
                                                Dashboard
                                            </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/Course-Approval" class="nav-link menu-link">
                                            <i class="mdi mdi-speedometer"></i>
                                            <span>
                                                Courses Approval
                                            </span>
                                        </Link>
                                    </li>

                                    <li class="nav-item">
                                        <Link to="/Holidays-list" class="nav-link menu-link">
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span>
                                                Holiday List
                                            </span>
                                        </Link>

                                    </li>
                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/Course-Attandance"  >
                                            <i class="mdi mdi-calendar-month"></i>
                                            <span> Courses Attendance</span>
                                        </Link>
                                    </li>

                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/Pymnt-Com" >
                                            <i class="mdi mdi-view-grid-plus-outline"></i>
                                            <span>Payment Complete Student List</span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/Pymnt-not-Com" >
                                            <i class="bx bxs-badge-dollar"></i>
                                            <span>Payment Not Complete Student List</span>
                                        </Link>
                                    </li>

                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/Course-Opted" >
                                            <i class="ri-clipboard-line"></i>
                                            <span>Courses opted list</span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link class="nav-link menu-link" to="/All-Courses">
                                            <i class="ri-align-justify"></i>
                                            <span> All Courses Status</span>
                                        </Link>
                                    </li>

                                    <ul class="navbar-nav" id="navbar-nav">
                                        <li class="menu-title">
                                            <span>Coordinators</span>
                                        </li>
                                        <Link to="/Coordinators" class="nav-link menu-link">
                                            <i class="ri-user-line"></i>
                                            <span>
                                                Coordinator
                                            </span>
                                        </Link>
                                    </ul>
                                </>
                            )}

                            {/* Role 1 means Student */}

                            {(sessionRole === '1' || sessionRole === 1) && (

                                <>
                                    <li class="nav-item">
                                        <Link to="/Dashboard" class="nav-link menu-link">
                                            <i class="ri-home-fill"></i>
                                            <span>
                                                Dashboard
                                            </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/All-Courses" class="nav-link menu-link" >
                                            <i class="ri-list-check"></i>
                                            <span> All Courses </span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/Student-Cart" class="nav-link menu-link" >
                                            <i class="ri-grid-fill"></i>
                                            <span>Your Carts</span>
                                        </Link>
                                    </li>
                                    <li class="nav-item">
                                        <Link to="/YourCourses" class="nav-link menu-link" >
                                            <i class="ri-grid-fill"></i>
                                            <span>Your Courses</span>
                                        </Link>
                                    </li>
                                </>
                            )}
                        </ul>
                    </div>
                </div>
                <div class="sidebar-background"></div>
            </div>
        </>
    );
}
export default Base;